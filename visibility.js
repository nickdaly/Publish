function changeVisibility(checkbox) {
    items = document.getElementsByClassName(checkbox.name);

    for (var i = 0; i < items.length; i++) {
        items[i].style.display = (checkbox.checked) ? "table-cell" : "none";
    }
}
