#!/bin/sh

# Inspired by http://orgmode.org/manual/Batch-execution.html

HERE=`pwd`
FILES=""
LOADPATH="/usr/share/emacs/site-lisp/org-mode/"
ORGINSTALL="${LOADPATH}/org-install.el"

# allow passing "emacsclient"
if [ -z "$EMACS" ]
then
    executable="emacs -Q --batch -l $ORGINSTALL"
else
    executable=$EMACS
fi

# escape all the files in quotes.
for i in $@; do
    FILES="$FILES \"$i\""
done

# run the Emacs!
$executable \
--eval "(progn
(add-to-list 'load-path (expand-file-name \"$LOADPATH\"))
(require 'org)
(mapc (lambda (file)
       (find-file (expand-file-name file \"$HERE\"))
       (org-export-as-html 3 nil nil nil nil nil)
       (kill-buffer)) '($FILES)))" > /dev/null
