#! /bin/sh

# hosting.sh: Self-publish projects.
#
# Copyright (C) 2012  Nick Daly <Nick.M.Daly@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# set default values
myport=8000
mytime=300
mytargets="issues/index.html wiki/index.html ../current.tar.gz"
myexcludes=""
here=`pwd`

# die on error
set -e

# override defaults when specified
if [ -n "$port" ]
then
    myport=$port
fi
if [ -n "$time" ]
then
    mytime=$time
fi
if [ -n "$targets" ]
then
    mytargets=$targets
fi
if [ -n "$excludes" ]
then
    myexcludes=$excludes
fi

#
# host!
#

if [ "$port" != "-1" ]
then
    cd ..; python -m SimpleHTTPServer $myport &
    cd $here
else
    echo "Port = -1: Not serving files."
fi

while [ 1 ]
do
    make EXCLUDE-FILES="$myexcludes" $mytargets
    echo "Sleeping for ${mytime} seconds from `date` to `date -d +${mytime}seconds`."
    sleep $mytime
done
