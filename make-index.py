#! /usr/bin/python

"""Creates an index file displaying the metadata in each org file."""

import cgi
from collections import defaultdict as DefaultDict
import logging
import os
import sys

from Cheetah.Template import Template

def main():
    """Find the files to process."""

    adir = sys.argv[1]

    supported = lambda afile: True in [
        afile.endswith(x) for x in [".org", ".rst", ".mkdn"]]

    files = [ adir + os.sep + afile for afile in sorted(os.listdir(adir))
          if supported(afile) and not afile.startswith("template") ]

    parse_files(adir, files)

def parse_files(adir, files):
    """Flow control for parsing all org files."""

    file_data = list()
    metadata_keys = set()
    output = adir + os.sep + "index.html"
    new = ".new"

    for org_file in files:

        try:
            with open(org_file) as infile:
                metadata = parse_org_file(infile)

                file_data.append(metadata)

                metadata_keys |= set(file_data[-1].keys())
        except IOError:
            logging.error("Could not read %s" % org_file)

    metadata_keys ^= set(["name"])
    metadata_keys = set([cgi.escape(key) for key in metadata_keys])

    # write to a temporary file.
    try:
        with open(output + new, "w") as outfile:
            outfile.write(
                str(Template(file="index.tmpl",
                             searchList={"file_data": file_data,
                                         "metadata_keys": metadata_keys,
                                         "sep": os.sep,
                                         "name": adir.capitalize()})))
    except IOError:
        logging.error("Could not write %s" % html_file)
    else:
        # if the write succeeded, replace the old file with the new one.
        os.rename(output + new, output)

def parse_org_file(infile):
    """Parse metadata from syntactically-correct org files.

    Extract properties from "* Metadata" sections.

    """
    in_metadata = 0
    literal_section_count = 0
    in_properties = 0
    properties = DefaultDict(str)

    properties["name"] = infile.name

    for line in infile.readlines():
        line = line.split()

        if not line:
            continue

        if line[0].upper().startswith("#+BEGIN_"):
            literal_section_count += 1
            continue
        elif line[0].upper().startswith("#+END_"):
            literal_section_count -= 1
            continue
        elif line[0].upper() == ":PROPERTIES:":
            in_properties = 1
            continue
        elif line[0].upper() == ":END:":
            in_properties = 0
            continue
        elif line[0] == "*":
            if line[1].upper() == "METADATA":
                in_metadata = 1
            else:
                in_metadata = 0
            continue

        if in_metadata and in_properties and literal_section_count == 0:
            properties[line[0].strip(":")] = " ".join(line[1:])

    return properties

if __name__ == "__main__":
    main()
