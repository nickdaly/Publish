#! /usr/bin/make

issues-org-html := $(patsubst %.org,%.html,$(wildcard issues/*.org))
issues-rst-html := $(patsubst %.rst,%.html,$(wildcard issues/*.rst))
issues-mkdn-html := $(patsubst %.mkdn,%.html,$(wildcard issues/*.mkdn))

wiki-org-html := $(patsubst %.org,%.html,$(wildcard wiki/*.org))
wiki-rst-html := $(patsubst %.rst,%.html,$(wildcard wiki/*.rst))
wiki-mkdn-html := $(patsubst %.mkdn,%.html,$(wildcard wiki/*.mkdn))

issues-html := $(issues-org-html) $(issues-rst-html) $(issues-mkdn-html)
wiki-html := $(wiki-org-html) $(wiki-rst-html) $(wiki-mkdn-html)

everything := $(wildcard ../*)

all: issues/index.html wiki/index.html ../current.tar.gz

%.html: %.org
	bash ./org-export-as-html.sh $<

%.html: %.rst
	rst2html $< > $@

%.html: %.mkdn
	markdown $< > $@

issues/index.html: $(issues-html) org-export-as-html.sh make-index.py index.tmpl
	python ./make-index.py issues

wiki/index.html: $(wiki-html) org-export-as-html.sh make-index.py index.tmpl
	python ./make-index.py wiki

# we can be lazy because make handles circiular dependencies beautifully.
../current.tar.gz: $(everything)
	tar cz --exclude=../current.tar.gz --exclude="$(EXCLUDE-FILES)" \
		--exclude=*~ ../ > ../current.tar.gz
